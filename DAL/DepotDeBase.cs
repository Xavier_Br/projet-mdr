﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using EF;

namespace DAL
{
    public abstract class DepotDeBase<TEntity> : IDisposable where TEntity : class
    {
        private bool _alreadyDisposed = false;

        internal ContexteDuProjet _context;

        public bool AutoSave { get; set; }

        public bool EnableLazyLoading
        {
            get
            {
                return _context.Configuration.LazyLoadingEnabled;
            }
            set
            {
                _context.Configuration.LazyLoadingEnabled = value;
            }
        }

        internal DepotDeBase() : this(false)
        {

        }

        internal DepotDeBase(bool isLogActive)
        {
            if (isLogActive)
            {
                _context.Database.Log = Console.WriteLine;
            }

            AutoSave = false;
        }



        public virtual void Add(TEntity nouvEntite)
        {
            //verif que je tente pas d'ajouter une commande
            //appartenant au contexte.
            if (_context.Entry(nouvEntite).State
                != System.Data.Entity.EntityState.Detached)
            {
                throw new ArgumentException("l'entité est déja rattachée au contexte");
            }

            _context.Set<TEntity>().Add(nouvEntite);
            CheckAutosave();
        }



        public void Delete(TEntity entitéASupprimer)
        {
            //si mon entité n'est pas gérée par le contexte courant
            CheckAndAttachEntity(entitéASupprimer);

            _context.Set<TEntity>().Remove(entitéASupprimer);
            CheckAutosave();
        }

        public virtual void Update(TEntity entiteAMettreAJour)
        {
            if (CheckAndAttachEntity(entiteAMettreAJour))
            {
                //je dois dire au contexte que l'entité
                //rattachée doit être persistée
                _context.Entry(entiteAMettreAJour).State
                    = System.Data.Entity.EntityState.Modified;
            }
            CheckAutosave();
        }

        public void PersistChanges()
        {
            _context.SaveChanges();
        }

        private bool CheckAndAttachEntity(TEntity entity)
        {
            if (_context.Entry(entity).State
                            == System.Data.Entity.EntityState.Detached)
            {
                //je rattache l'entité au contexte
                _context.Set<TEntity>().Attach(entity);
                return true;
            }

            return false;
        }

        protected List<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        private void CheckAutosave()
        {
            if (AutoSave)
                this.PersistChanges();
        }


        #region Pattern IDisposable
        protected virtual void FreeResources(bool isDisposing)
        {
            if (isDisposing)
            {
                _context.Dispose();
            }
        }


        public void Dispose()
        {
            if (!_alreadyDisposed)
            {
                FreeResources(true);
                GC.SuppressFinalize(this);
                _alreadyDisposed = true;
            }
        }

        ~DepotDeBase()
        {
            FreeResources(false);
        }
        #endregion

    }

}
