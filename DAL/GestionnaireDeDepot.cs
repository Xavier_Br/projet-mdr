﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;
using DAL.Depots;

namespace DAL
{
    public class GestionnaireDeDepot : IDisposable
    {
        private ContexteDuProjet _context = new ContexteDuProjet();

        private DepotAnnonces _depotAnnonce;
        private DepotAnnonceStatut _depotAnnonceStatut;
        private DepotArbreNoeud _depotArbreNoeud;
        private DepotBlocDeTexte _depotBlocDeTexte;
        private DepotBDTType _depotBDTType;
        private DepotCandidats _depotCandidats;
        private DepotCandidatures _depotCandidatures;
        private DepotCandidatureExperience _depotCandidatureExperience;
        private DepotCandidaturePoste _depotCandidaturePoste;
        private DepotCandidatureStatut _depotCandidatureStatut;
        private DepotCandidatureProvenance _depotCandidatureProvenance;
        private DepotMailTemplates _depotMailTemplates;


        private bool _alreadyDisposed;

        public DepotAnnonces DepotAnnonces
        {
            get
            {
                if (_depotAnnonce == null)
                {
                    _depotAnnonce = new DepotAnnonces();
                    _depotAnnonce._context = this._context;
                }

                return _depotAnnonce;
            }
        }

        public DepotAnnonceStatut DepotAnnonceStatut
        {
            get
            {
                if (_depotAnnonceStatut == null)
                {
                    _depotAnnonceStatut = new DepotAnnonceStatut();
                    _depotAnnonceStatut._context = this._context;
                }

                return _depotAnnonceStatut;
            }

        }

        public DepotArbreNoeud DepotArbreNoeud
        {
            get
            {
                if (_depotArbreNoeud == null)
                {
                    _depotArbreNoeud = new DepotArbreNoeud();
                    _depotArbreNoeud._context = this._context;
                }

                return _depotArbreNoeud;
            }

        }

        public DepotBlocDeTexte DepotBlocDeTexte
        {
            get
            {
                if (_depotBlocDeTexte == null)
                {
                    _depotBlocDeTexte = new DepotBlocDeTexte();
                    _depotBlocDeTexte._context = this._context;
                }

                return _depotBlocDeTexte;
            }

        }


        public DepotBDTType DepotBDTType
        {
            get
            {
                if (_depotBDTType == null)
                {
                    _depotBDTType = new DepotBDTType();
                    _depotBDTType._context = this._context;
                }

                return _depotBDTType;
            }
        }

        public DepotCandidats DepotCandidats
        {
            get
            {
                if (_depotCandidats == null)
                {
                    _depotCandidats = new DepotCandidats();
                    _depotCandidats._context = this._context;
                }

                return _depotCandidats;
            }
        }

        public DepotCandidatures DepotCandidatures
        {
            get
            {
                if (_depotCandidatures == null)
                {
                    _depotCandidatures = new DepotCandidatures();
                    _depotCandidatures._context = this._context;
                }

                return _depotCandidatures;
            }
        }


        public DepotCandidatureExperience DepotCandidatureExperience
        {
            get
            {
                if (_depotCandidatureExperience == null)
                {
                    _depotCandidatureExperience = new DepotCandidatureExperience();
                    _depotCandidatureExperience._context = this._context;
                }

                return _depotCandidatureExperience;
            }
        }

        public DepotCandidaturePoste DepotCandidaturePoste
        {
            get
            {
                if (_depotCandidaturePoste == null)
                {
                    _depotCandidaturePoste = new DepotCandidaturePoste();
                    _depotCandidaturePoste._context = this._context;
                }

                return _depotCandidaturePoste;
            }
        }

        public DepotCandidatureStatut DepotCandidatureStatut
        {
            get
            {
                if (_depotCandidatureStatut == null)
                {
                    _depotCandidatureStatut = new DepotCandidatureStatut();
                    _depotCandidatureStatut._context = this._context;
                }

                return _depotCandidatureStatut;
            }
        }

        public DepotCandidatureProvenance DepotCandidatureProvenance
        {
            get
            {
                if (_depotCandidatureProvenance == null)
                {
                    _depotCandidatureProvenance = new DepotCandidatureProvenance();
                    _depotCandidatureProvenance._context = this._context;
                }

                return _depotCandidatureProvenance;
            }
        }

        public DepotMailTemplates DepotMailTemplates
        {
            get
            {
                if (_depotMailTemplates == null)
                {
                    _depotMailTemplates = new DepotMailTemplates();
                    _depotMailTemplates._context = this._context;
                }

                return _depotMailTemplates;
            }
        }



        #region Pattern IDisposable
        protected virtual void FreeResources(bool isDisposing)
        {
            if (isDisposing)
            {
                _context.Dispose();
                _depotAnnonce?.Dispose();
                _depotAnnonceStatut?.Dispose();
                _depotArbreNoeud?.Dispose();
                _depotBlocDeTexte?.Dispose();
                _depotBDTType?.Dispose();
                _depotCandidats?.Dispose();
                _depotCandidatures?.Dispose();
                _depotCandidatureExperience?.Dispose();
                _depotCandidaturePoste?.Dispose();
                _depotCandidatureStatut?.Dispose();
                _depotCandidatureProvenance?.Dispose();
                _depotMailTemplates?.Dispose();


            }
        }


        public void Dispose()
        {
            if (!_alreadyDisposed)
            {
                FreeResources(true);
                GC.SuppressFinalize(this);
                _alreadyDisposed = true;
            }
        }

        ~GestionnaireDeDepot()
        {
            FreeResources(false);
        }
        #endregion
    }
}

