﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotArbreNoeud : DepotDeBase<ArbreNoeud>
    {
        public List<ArbreNoeud> ListerNoeuds()
        {
            return this.GetAll();
        }

        public ArbreNoeud Arbre()
        {
            List<ArbreNoeud> Noeuds = ListerNoeuds();
            return Noeuds.Where(n => n.NoeudParent == null).First();
        }
    }
}
