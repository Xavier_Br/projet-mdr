﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotCandidaturePoste : DepotDeBase<CandidaturePoste>
    {
        public List<CandidaturePoste> ListerPostes()
        {
            return this.GetAll();
        }

        public CandidaturePoste PosteParId(int id)
        {
            return _context.CandidaturePostes.Find(id);
        }
    }
}
