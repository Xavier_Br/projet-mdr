﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotAnnonceStatut : DepotDeBase<AnnonceStatut>
    {
        public List<AnnonceStatut> ListerStatuts()
        {
            return this.GetAll();
        }
    }
}
