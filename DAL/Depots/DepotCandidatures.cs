﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL.Depots
{
    public class DepotCandidatures : DepotDeBase<Candidature>
    {
        public List<Candidature> ListerToutesLesCandidatures()
        {
            return _context.Candidatures.Include("Candidat").Include("Statuts").ToList();
        }

        public List<Candidature> CandidaturesContenant(string filtre)
        {
            var chercher = from c in _context.Candidatures
                           where c.Candidat.Nom.Contains(filtre) || c.Candidat.Prenom.Contains(filtre) || c.ReferenceAnnonce.Contains(filtre)
                           select c;

            return chercher.ToList();
        }

        public List<Candidature> CandidaturesParImportance(bool importance)
        {
            var chercher = from c in _context.Candidatures
                           where c.FlagImportance == importance
                           select c;

            return chercher.ToList();
        }

        //public List<Candidature> RechercheParPosteEtTechnoEtExperience(string poste, string techno, string experience)
        //{

        //    var chercher = from c in _context.Candidatures
        //                   where c.Poste.Intitule.Contains(poste) && c.Techno.Contains(techno) && c.Experience.Intitule.Contains(experience)
        //                   select c;

        //    return chercher.ToList();
        //}

        public override void Add(Candidature nouvCandidature)
        {
            if (nouvCandidature.Statuts == null)
                nouvCandidature.Statuts = new List<CandidatureStatut>();
            nouvCandidature.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now, ModifiePar="Admin"});
            base.Add(nouvCandidature);
        }

        public List<Candidature> RechercheToutArguments(string nom, string prenom, string poste, string techno, string experience, string provenance, string refannonce)
        {
            var chercher = from c in _context.Candidatures.Include("Candidat").Include("Statuts")
                           where c.Candidat.Nom.Contains(nom)
                                 && c.Candidat.Prenom.Contains(prenom)
                                 && (poste == "" || (c.Poste != null && c.Poste.Contains(poste)))
                                 && (techno == "" || (c.Techno != null && c.Techno.Contains(techno)))
                                 && (experience == "" || (c.Experience != null && c.Experience.Contains(experience)))
                                 && (provenance == "" || (c.Provenance != null && c.Provenance.Contains(provenance)))
                                 && (refannonce == "" || (c.ReferenceAnnonce != null && c.ReferenceAnnonce.Contains(refannonce)))
                           select c;

            return chercher.ToList().OrderByDescending(c => c.Statuts.Min(s => s.ModifieLe).Ticks).ToList();
        }

        public Candidature RechercheCandidatureParId(int id)
        {
            _context.Configuration.LazyLoadingEnabled = false;
            var chercher = from c in _context.Candidatures.Include("Candidat").Include("Statuts")
                           where c.Id.Equals(id)
                           select c;
            return chercher.ToList().First();

        }

        public List<Candidature> RechercheSimple(string motclé){
            var chercher = from c in _context.Candidatures.Include("Candidat").Include("Statuts")
                           where c.Candidat.Nom.Contains(motclé)
                                 || c.Candidat.Prenom.Contains(motclé)
                                 || (c.Poste != null && c.Poste.Contains(motclé))
                                 || (c.Techno != null && c.Techno.Contains(motclé))
                                 || (c.Experience != null && c.Experience.Contains(motclé))
                                 || (c.Provenance != null && c.Provenance.Contains(motclé))
                                 || (c.ReferenceAnnonce != null && c.ReferenceAnnonce.Contains(motclé))
                           select c;

            return chercher.ToList().OrderByDescending(c => c.Statuts.Min(s => s.ModifieLe).Ticks).ToList();
        }


    }
}
