﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Depots
{
    public class DepotMailTemplates : DepotDeBase<MailTemplate>
    {

        public List<MailTemplate> ListerTemplates()
        {
            return this.GetAll();
        }

        public MailTemplate MailTemplateParId(int id)
        {
            return _context.MailTemplates.Find(id);
        }
    }

}
