﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL.Depots
{
    public class DepotCandidats : DepotDeBase<Candidat>
    {
        public List<Candidat> ListerTousLesCandidats()
        {
            return this.GetAll();
        }

        public List<Candidat> CandidatsSimilaire(string nom, string prenom, DateTime dateDeNaissance)
        {
            var chercher = from c in _context.Candidats
                           where c.Nom == nom && c.Prenom == prenom && c.DateDeNaissance == dateDeNaissance
                           select c;

            return chercher.ToList();
        }

        public List<Candidat> CandidatModifiePar(string nom)
        {
            var chercher = from c in _context.Candidats
                           where c.ModifiePar.Contains(nom)
                           select c;

            return chercher.ToList();
        }

        public Candidat CandidatParId(int id)
        {
            return _context.Candidats.Find(id);
        }


        public override void Add(Candidat nouvCandidat)
        {
            nouvCandidat.CreeLe = DateTime.Now;
            nouvCandidat.CreePar = "Admin";
            nouvCandidat.ModifieLe = DateTime.Now;
            nouvCandidat.ModifiePar = "Admin";
            base.Add(nouvCandidat);
        }

        public override void Update(Candidat candidatAMettreAJour)
        {
            candidatAMettreAJour.ModifieLe = DateTime.Now;
            candidatAMettreAJour.ModifiePar = "Admin";
            base.Update(candidatAMettreAJour);
        }

        public Candidat RechercherCandidatParId(int id)
        {
            var chercher = from c in _context.Candidats.Include("Candidatures").Include("Candidatures.Statuts")
                           where c.Id == id
                           select c;

            return chercher.ToList().First();
        }
    }
}
