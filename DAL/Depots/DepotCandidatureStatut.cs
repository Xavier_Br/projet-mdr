﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotCandidatureStatut : DepotDeBase<CandidatureStatut>
    {
        public List<CandidatureStatut> ListerStatutsDeToutesCandidatures()
        {
            return this.GetAll();
        }

        public List<string> ListerStatuts()
        {
            var chercher = from c in _context.CandidatureStatuts
                           select c.NomStatut;

            return chercher.Distinct().ToList();
        }
    }
}
