﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotCandidatureProvenance : DepotDeBase<CandidatureProvenance>
    {
        public List<CandidatureProvenance> ListerProvenances()
        {
            return this.GetAll();
        }

        public CandidatureProvenance ProvenanceParId(int id)
        {
            return _context.CandidatureProvenances.Find(id);
        }
    }
}
