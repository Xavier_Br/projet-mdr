﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotBDTType : DepotDeBase<TypeBDT>
    {
        public List<TypeBDT> ListerTypes()
        {
            return this.GetAll();
        }

        public TypeBDT RecupererTypeParId(int id)
        {
            return _context.BDTTypes.Find(id);

        }
    }
}
