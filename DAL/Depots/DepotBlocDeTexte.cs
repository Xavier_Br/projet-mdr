﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;


namespace DAL.Depots
{
    public class DepotBlocDeTexte : DepotDeBase<BlocDeTexte>
    {
        public List<BlocDeTexte> RecupereTousLesBlocsDeTexte()
        {
            return this.GetAll();
        }

        public List<BlocDeTexte> RechercherNom(string nom)
        {
            var chercher = from bdt in _context.BlocsDeTexte
                            where bdt.Nom.Contains(nom)
                            select bdt;

            return chercher.ToList();
        }

        public List<BlocDeTexte> RechercherLes5PlusUtilises()
        {
            var chercher = (from bdt in _context.BlocsDeTexte
                            select bdt).OrderByDescending(c => c.NbAcces).Take(5);

            return chercher.ToList();
        }

        public List<BlocDeTexte> RechercheModifiePar(string utilisateur)
        {
            var chercher = from a in _context.BlocsDeTexte
                           where a.ModifiePar.Contains(utilisateur)
                           select a;

            return chercher.ToList();
        }

        public List<BlocDeTexte> RechercherLes5PlusRecents()
        {
            var chercher = (from bdt in _context.BlocsDeTexte
                            select bdt).OrderByDescending(c => c.ModifieLe).Take(5);

            return chercher.ToList();
        }

        public override void Add(BlocDeTexte nouvBDT)
        {
            nouvBDT.CreeLe = DateTime.Now;
            nouvBDT.CreePar = "Admin";
            nouvBDT.ModifieLe = DateTime.Now;
            nouvBDT.ModifiePar = "Admin";
            base.Add(nouvBDT);
        }

        public override void Update(BlocDeTexte BDTAMettreAJour)
        {
            BDTAMettreAJour.ModifieLe = DateTime.Now;
            BDTAMettreAJour.ModifiePar = "Admin";
            base.Update(BDTAMettreAJour);
        }

        public List<BlocDeTexte> RechercherBDTParTypeEtFiltre(string intituleType, string filtre)
        {
            var chercher = from bdt in _context.BlocsDeTexte.Include("Type")
                           where (bdt.Nom.Contains(filtre) && bdt.Type.Intitule.Equals(intituleType))
                           select bdt;

            return chercher.ToList().OrderByDescending(b => b.NbAcces).ToList();
        }

        public BlocDeTexte RechercherParId(int id)
        {

            var query = from b in _context.BlocsDeTexte.Include("Type")
                    where b.Id==id
                    select b;
            return query.ToList().First();
        }
    }
}
