﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;


namespace DAL.Depots
{
    public class DepotAnnonces : DepotDeBase<Annonce>
    {
        public List<Annonce> RecupereToutesLesAnnonces()
        {
            return this.GetAll();
        }

        public List<string> RecupereTousLesAuteurs()
        {
            var auteurs = from a in _context.Annonces
                          select a.CreePar;

            return auteurs.Distinct().ToList();
        }

        public Annonce RecupererAnnonceParId(int id)
        {
            var chercher = from a in _context.Annonces.Include("LignesAnnonce").Include("LignesAnnonce.BlocDeTexte").Include("Statut")
                           where a.Id == id
                           select a;
            return chercher.ToList().First();

        }
        
        public List<Annonce> RechercherCinqDernieresAnnonces()
        {
            var chercher = (from a in _context.Annonces
                            select a).OrderByDescending(d => d.ModifieLe).Take(5);

            return chercher.ToList();
        }

        public List<Annonce> RechercheAnnonceParTitreouRefouAuteur(string utilisateur, string TitreOuRef)
        {
            var chercher = (from a in _context.Annonces
                           where a.CreePar.Contains(utilisateur) && (a.Titre.Contains(TitreOuRef) || a.Reference.Contains(TitreOuRef) || a.Nom.Contains(TitreOuRef))
                           select a).OrderByDescending(d => d.ModifieLe);

            return chercher.ToList();
        }
    

        public override void Add(Annonce nouvAnnonce)
        {
            nouvAnnonce.CreeLe = DateTime.Now;
            nouvAnnonce.CreePar = "Admin";
            nouvAnnonce.ModifieLe = DateTime.Now;
            nouvAnnonce.ModifiePar = "Admin";
            base.Add(nouvAnnonce);
        }

        public override void Update(Annonce annonceAMettreAJour)
        {
            annonceAMettreAJour.ModifieLe = DateTime.Now;
            annonceAMettreAJour.ModifiePar = "Admin";
            base.Update(annonceAMettreAJour);
        }
    }
}
