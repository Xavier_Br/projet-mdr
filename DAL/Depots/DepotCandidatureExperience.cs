﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF;
using Model;

namespace DAL.Depots
{
    public class DepotCandidatureExperience : DepotDeBase<CandidatureExperience>
    {
        public List<CandidatureExperience> ListerExperiences()
        {
            return this.GetAll();
        }

        public CandidatureExperience ExperienceParId(int id)
        {
            return _context.CandidatureExperiences.Find(id);
        }
    }
}
