﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace EF
{
    class InitialiseurDeLaBaseDeDonnees : DropCreateDatabaseAlways<ContexteDuProjet>
    {
        protected override void Seed(ContexteDuProjet context)
        {
            MailTemplate mail = new MailTemplate();
            mail.Nom = "Refus Type";
            mail.Corps = @"Madame ou Monsieur,

En réponse à votre candidature, je suis au regret de devoir vous informer que celle-ci n'a pas été retenue. Soyez cependant assuré que cette décision ne met pas en cause vos qualités personnelles, ni même celles de votre formation.

Nous sommes très sensibles à l'intérêt que vous portez à notre entreprise, et conservons vos coordonnées afin de vous recontacter au besoin. Nous vous souhaitons une pleine réussite dans vos recherches futures.

Veuillez agréer, Madame/Monsieur, l'expression de mes salutations distinguées."; 
            mail.Objet = "Réponse à candidature SOPRA";
            context.MailTemplates.Add(mail);

            MailTemplate mail2 = new MailTemplate();
            mail2.Nom = "Demande d'entretien à personnaliser";
            mail2.Corps = @"Nous avons bien reçu votre offre de candidature au sein de notre entreprise pour le poste ____ ( préciser le libellé du poste ) et nous vous en remercions.

Afin d'examiner votre candidature de manière plus complète, nous souhaiterions vous rencontrer.

Aussi, nous vous proposons un rendez-vous en nos locaux avec ____ , responsable du service de ____ , le ____ à ____ heures.

Si cette date ne vous convenait pas, nous vous remercions de  bien vouloir contacter M ____au numéro ____ afin de convenir d'un nouveau rendez-vous.

Nous vous prions de croire, M ____ , en l'expression de nos sincères salutations.";
            mail2.Objet = "Proposition d'entretien";
            context.MailTemplates.Add(mail2);



            //Ajout de type de bdt

            //Candidature Experience
            TypeBDT type1 = new TypeBDT();
            type1.Intitule = "Présentation de l'entreprise";
            context.BDTTypes.Add(type1);

            TypeBDT type2 = new TypeBDT();
            type2.Intitule = "Mission";
            context.BDTTypes.Add(type2);

            TypeBDT type3 = new TypeBDT();
            type3.Intitule = "Profil";
            context.BDTTypes.Add(type3);


            //// ajout de 5 annonces 
            //Annonce an = new Annonce();
            //an.Nom = "2013-Annonce1";
            //an.Titre = "titre 0012";
            //an.CreeLe = DateTime.Now;
            //an.ModifieLe = DateTime.Now;
            //if (an.LignesAnnonce == null)
            //    an.LignesAnnonce = new List<LigneAnnonce>();
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 1", Nom = "2013-Annonce1bdt1", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now,NbAcces=2,Type=type1 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 2", Nom = "2013-Annonce1bdt2", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 3,Type = type2 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 3", Nom = "2013-Annonce1bdt3", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 4, Type = type3 });
            //an.CreePar = "Auteur1";
            //context.Annonces.Add(an);

            //an = new Annonce();
            //an.Nom = "2013-Annonce2";
            //an.Titre = "titre 0123";
            //an.CreeLe = DateTime.Now;
            //an.ModifieLe = DateTime.Now;
            //if (an.BlocsDeTexte == null)
            //    an.BlocsDeTexte = new List<BlocDeTexte>();
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 1", Nom = "2013-Annonce2bdt1", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now,NbAcces = 2, Type = type1 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 2", Nom = "2013-Annonce2bdt2", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type2 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 3", Nom = "2013-Annonce2bdt3", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type3 });
            //an.CreePar = "Auteur2";
            //context.Annonces.Add(an);

            //an = new Annonce();
            //an.Nom = "2014-Annonce23";
            //an.Titre = "titre 1234";
            //an.CreeLe = DateTime.Now;
            //an.ModifieLe = DateTime.Now;
            //if (an.BlocsDeTexte == null)
            //    an.BlocsDeTexte = new List<BlocDeTexte>();
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 1", Nom = "2014-Annonce23bdt1", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type1 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 2", Nom = "2014-Annonce23bdt2", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type2 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 3", Nom = "2014-Annonce23bdt3", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type3 });
            //an.CreePar = "Auteur1";
            //context.Annonces.Add(an);

            //an = new Annonce();
            //an.Nom = "2015-Annonce3";
            //an.Titre = "titre 2345";
            //an.CreeLe = DateTime.Now;
            //an.ModifieLe = DateTime.Now;
            //if (an.BlocsDeTexte == null)
            //    an.BlocsDeTexte = new List<BlocDeTexte>();
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 1", Nom = "2015-Annonce3bdt1", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type1 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 2", Nom = "2015-Annonce3bdt2", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type2 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 3", Nom = "2015-Annonce3bdt3", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type3 });
            //an.CreePar = "Auteur3";
            //context.Annonces.Add(an);

            //an = new Annonce();
            //an.Nom = "2016-Annonce43";
            //an.Titre = "titre 3456";
            //an.CreeLe = DateTime.Now;
            //an.ModifieLe = DateTime.Now;
            //if (an.BlocsDeTexte == null)
            //    an.BlocsDeTexte = new List<BlocDeTexte>();
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 1", Nom = "2016-Annonce43bdt1", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type1 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 2", Nom = "2016-Annonce43bdt2", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type2 });
            //an.BlocsDeTexte.Add(new BlocDeTexte { Texte = "texte 3", Nom = "2016-Annonce43bdt3", ModifieLe = DateTime.Now, CreeLe = DateTime.Now, DernierAccesLe = DateTime.Now, NbAcces = 2, Type = type3 });
            //an.CreePar = "Auteur3";
            //context.Annonces.Add(an);

            //Ajout des options de liste déroulantes

            //Candidature Experience
            CandidatureExperience ce = new CandidatureExperience();
            ce.Intitule = "Débutant";
            context.CandidatureExperiences.Add(ce);

            ce = new CandidatureExperience();
            ce.Intitule = "Expérimenté";
            context.CandidatureExperiences.Add(ce);

            ce = new CandidatureExperience();
            ce.Intitule = "Jeune Diplomé";
            context.CandidatureExperiences.Add(ce);

            //Candidature Poste
            CandidaturePoste cp = new CandidaturePoste();
            cp.Intitule = "Architecte";
            context.CandidaturePostes.Add(cp);

            cp = new CandidaturePoste();
            cp.Intitule = "Consultant";
            context.CandidaturePostes.Add(cp);

            cp = new CandidaturePoste();
            cp.Intitule = "Chef de projet";
            context.CandidaturePostes.Add(cp);

            cp = new CandidaturePoste();
            cp.Intitule = "Développeur";
            context.CandidaturePostes.Add(cp);

            //Candidature Provenance
            CandidatureProvenance cpr = new CandidatureProvenance();
            cpr.Intitule = "Cooptée";
            context.CandidatureProvenances.Add(cpr);

            cpr = new CandidatureProvenance();
            cpr.Intitule = "Spontanée";
            context.CandidatureProvenances.Add(cpr);

            cpr = new CandidatureProvenance();
            cpr.Intitule = "Réponse à Offre";
            context.CandidatureProvenances.Add(cpr);


            //Ajout de 5 Candidats et leurs candidatures

            Candidat c = new Candidat();
            c.CreeLe = DateTime.Now;
            c.ModifieLe = DateTime.Now;
            c.DateDeNaissance = DateTime.Now.Date;
            c.Nom = "paulo";
            c.Prenom = "carlo";
            c.Email = c.Nom + "." + c.Prenom + "@sopra.com";
            c.Telephone = "0000002";
            Candidature candi = new Candidature();
            candi.Techno = "jquery et IIS";
            candi.Statuts = new List<CandidatureStatut>();
            candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
            if (c.Candidatures == null)
                c.Candidatures = new List<Candidature>();

            c.Candidatures.Add(candi);
            context.Candidats.Add(c);

            c = new Candidat();
            c.CreeLe = DateTime.Now;
            c.ModifieLe = DateTime.Now;
            c.DateDeNaissance = DateTime.Now.Date;
            c.Nom = "marco";
            c.Prenom = "jean";
            c.Email = c.Nom + "." + c.Prenom + "@sopra.com";
            c.Telephone = "0000012";
            candi = new Candidature();
            candi.Techno = "angularJS";
            candi.Statuts = new List<CandidatureStatut>();
            candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
            if (c.Candidatures == null)
                c.Candidatures = new List<Candidature>();
            c.Candidatures.Add(candi);
            context.Candidats.Add(c);

            c = new Candidat();
            c.CreeLe = DateTime.Now;
            c.ModifieLe = DateTime.Now;
            c.DateDeNaissance = DateTime.Now.Date;
            c.Nom = "caule";
            c.Prenom = "jerome";
            c.Email = c.Nom + "." + c.Prenom + "@sopra.com";
            c.Telephone = "0001012";
            candi = new Candidature();
            candi.Techno = "java et c#";
            candi.Statuts = new List<CandidatureStatut>();
            candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
            if (c.Candidatures == null)
                c.Candidatures = new List<Candidature>();
            c.Candidatures.Add(candi);
            context.Candidats.Add(c);

            c = new Candidat();
            c.CreeLe = DateTime.Now;
            c.ModifieLe = DateTime.Now;
            c.DateDeNaissance = DateTime.Now.Date;
            c.Nom = "clement";
            c.Prenom = "clement";
            c.Email = c.Nom + "." + c.Prenom + "@sopra.com";
            c.Telephone = "000786012";
            candi = new Candidature();
            candi.Statuts = new List<CandidatureStatut>();
            candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
            candi.Techno = "java";
            if (c.Candidatures == null)
                c.Candidatures = new List<Candidature>();
            c.Candidatures.Add(candi);
            candi = new Candidature();
            candi.Statuts = new List<CandidatureStatut>();
            candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
            candi.Techno = "sharepoint";
            c.Candidatures.Add(candi);
            context.Candidats.Add(c);

            for (int i=0; i < 1; i++)
            {
                c = new Candidat();
                c.CreeLe = DateTime.Now;
                c.ModifieLe = DateTime.Now;
                c.DateDeNaissance = DateTime.Now.Date;
                c.Nom = "ferat";
                c.Prenom = "romain";
                c.Email = c.Nom + "." + c.Prenom + "@sopra.com";
                c.Telephone = "075670012";
                if (c.Candidatures == null)
                    c.Candidatures = new List<Candidature>();
                candi = new Candidature();
                candi.Statuts = new List<CandidatureStatut>();
                candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
                candi.Techno = "java";
                c.Candidatures.Add(candi);
                candi = new Candidature();
                candi.Statuts = new List<CandidatureStatut>();
                candi.Statuts.Add(new CandidatureStatut() { NomStatut = "créé", ModifieLe = DateTime.Now });
                candi.Techno = "c#";
                c.Candidatures.Add(candi);
                context.Candidats.Add(c);

            }
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
