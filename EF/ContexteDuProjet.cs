﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace EF
{
    public class ContexteDuProjet : DbContext
    {
        static ContexteDuProjet()
        {
            Database.SetInitializer(new InitialiseurDeLaBaseDeDonnees());

        }

        public DbSet<Annonce> Annonces { get; set; }

        public DbSet<AnnonceStatut> AnnonceStatuts { get; set; }

        public DbSet<BlocDeTexte> BlocsDeTexte { get; set; }

        public DbSet<TypeBDT> BDTTypes { get; set; }

        public DbSet<Candidat> Candidats { get; set; }

        public DbSet<Candidature> Candidatures { get; set; }

        public DbSet<CandidatureExperience> CandidatureExperiences { get; set; }

        public DbSet<CandidaturePoste> CandidaturePostes { get; set; }

        public DbSet<CandidatureStatut> CandidatureStatuts { get; set; }

        public DbSet<CandidatureProvenance> CandidatureProvenances { get; set; }

        public DbSet<ArbreNoeud> ArbreNoeuds { get; set; }

        public DbSet<CandidatureStatutType> CandidatureStatutTypes { get; set; }

        public DbSet<MailTemplate> MailTemplates { get; set; }

    }
}
