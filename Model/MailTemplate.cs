﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("MailTemplate")]
    public class MailTemplate
    {
        [Key]
        [Column("MAIL_TEMPLATE_ID")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("MAIL_TEMPLATE_NOM")]
        public string Nom { get; set; }

        [MaxLength(50)]
        [Column("MAIL_TEMPLATE_OBJET")]
        public string Objet { get; set; }

        [MaxLength(1500)]
        [Column("MAIL_TEMPLATE_CORPS")]
        public string Corps { get; set; }

    }
}

