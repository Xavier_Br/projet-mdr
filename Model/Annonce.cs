﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;

namespace Model
{
    [Table("ANNONCE")]
    public class Annonce
    {
        [Key]
        [Column("ANNONCE_ID")]
        public int Id {get;set;}

        [MaxLength(50)]
        [Column("ANNONCE_REFERENCE")]
        public string Reference {get;set;}

        [Required]
        [MaxLength(100)]
        [Column("ANNONCE_NOM")]
        public string Nom {get;set;}


        [MaxLength(100)]
        [Column("ANNONCE_TITRE")]
        public string Titre {get;set;}

        [MaxLength(50)]
        [Column("ANNONCE_CREEPAR")]
        public string CreePar { get; set; }

        [MaxLength(50)]
        [Column("ANNONCE_MODIFIEPAR")]
        public string ModifiePar { get; set; }

        [Column("ANNONCE_CREELE")]
        public DateTime CreeLe { get; set; }

        [Column("ANNONCE_MODIFIELE")]
        public DateTime ModifieLe { get; set; }

        //Propriété de Navigation

        public AnnonceStatut Statut {get;set;}

        public ICollection<LigneAnnonce> LignesAnnonce { get; set; }
    }
}