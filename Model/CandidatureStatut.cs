﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("CANDIDATURE_STATUT")]
    public class CandidatureStatut
    {
        [Key]
        [Column("CANDIDATURE_STATUT_ID")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("CANDIDATURE_STATUT_NOM")]
        public string NomStatut { get; set; }

        [Column("CANDIDAT_STATUT_MODIFIELE")]
        public DateTime ModifieLe { get; set; }

        [MaxLength(50)]
        [Column("CANDIDATURE_STATUT_MODIFIEPAR")]
        public string ModifiePar { get; set; }

        //[Column("CANDIDATURE_STATUT_CANDIDATURES")]
        public ICollection<Candidature> Candidatures { get; set; }

    }
}
