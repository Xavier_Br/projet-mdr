﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("BLOC_DE_TEXTE")]
    public class BlocDeTexte
    {
        [Key]
        [Column("BLOC_DE_TEXTE_ID")]
        public int Id { get; set; }

        [MaxLength(10000)]
        [Column("BLOC_DE_TEXTE_TEXTE")]
        public string Texte { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("BLOC_DE_TEXTE_NOM")]
        public string Nom { get; set; }

        [MaxLength(50)]
        [Column("BLOC_DE_TEXTE_CREEPAR")]
        public string CreePar { get; set; }

        [MaxLength(50)]
        [Column("BLOC_DE_TEXTE_MODIFIEPAR")]
        public string ModifiePar { get; set; }

        [Column("BLOC_DE_TEXTE_CREELE")]
        public DateTime CreeLe { get; set; }

        [Column("BLOC_DE_TEXTE_MODIFIELE")]
        public DateTime ModifieLe { get; set; }

        [Column("BLOC_DE_TEXTE_DERNIERACCESLE")]
        public DateTime DernierAccesLe { get; set; }

        [Column("BLOC_DE_TEXTE_NBACCES")]
        public int NbAcces { get; set; }


        //Propriété de Navigation
        public TypeBDT Type { get; set; }

        public ICollection<LigneAnnonce> LignesAnnonce { get; set; }

    }
}