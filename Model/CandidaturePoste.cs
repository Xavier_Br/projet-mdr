﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("CANDIDATURE_POSTE")]
    public class CandidaturePoste
    {
        [Key]
        [Column("CANDIDATURE_POSTE_ID")]
        public int Id { get; set; }

        [Required]
        [Column("CANDIDATURE_POSTE_INTITULE")]
        [MaxLength(50)]
        public string Intitule { get; set; }

        //[Column("CANDIDATURE_POSTE_CANDIDATURES")]
        public ICollection<Candidature> Candidatures { get; set; }
    }
}
