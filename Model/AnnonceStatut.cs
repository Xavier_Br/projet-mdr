﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("ANNONCE_STATUT")]
    public class AnnonceStatut
    {
        [Key]
        [Column("ANNONCE_STATUT_ID")]
        public int Id { get; set; }

        [MaxLength(50)]
        [Column("ANNONCE_STATUT_INTITULE")]
        [Required]
        public string Intitule { get; set; }

        //Propriété de Navigation
        public ICollection<Annonce> Annonces { get; set; }

    }
}