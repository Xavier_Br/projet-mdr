﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Model
{
    [Table("CANDIDAT")]
    public class Candidat
    {
        [Key]
        [Column("CANDIDAT_ID")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("CANDIDAT_NOM")]
        public string Nom { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("CANDIDAT_PRENOM")]
        public string Prenom { get; set; }

        [Required]
        [MaxLength(100)]
        [Column("CANDIDAT_EMAIL")]
        public string Email { get; set; }

        [MaxLength(20)]
        [Column("CANDIDAT_TELEPHONE")]
        public string  Telephone { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Column("CANDIDAT_DATEDENAISSANCE")]
        public DateTime DateDeNaissance { get; set; }

        [Column("CANDIDAT_COMMENTAIRE")]
        public string Commentaire { get; set; }

        [MaxLength(255)]
        [Column("CANDIDAT_DOSSIER")]
        public string Dossier { get; set; }

        [MaxLength(255)]
        [Column("CANDIDAT_CV")]
        public string CV { get; set; }

        [Column("CANDIDAT_CREELE")]
        public DateTime CreeLe { get; set; }

        [Column("CANDIDAT_MODIFIELE")]
        public DateTime ModifieLe { get; set; }

        [MaxLength(50)]
        [Column("CANDIDAT_CREEPAR")]
        public string CreePar { get; set; }

        [MaxLength(50)]
        [Column("CANDIDAT_MODIFIEPAR")]
        public string ModifiePar { get; set; }

        //propriété de navigation
        //[Column("CANDIDAT_CANDIDATURE")]
        public ICollection<Candidature> Candidatures { get; set; }

    }
}
