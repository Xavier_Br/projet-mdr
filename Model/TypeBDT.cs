﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("TYPE_BDT")]
    public class TypeBDT
    {
        [Key]
        [Column("TYPE_BDT_ID")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("TYPE_BDT_INTITULE")]
        public string Intitule { get; set; }

        //Propriété de Navigation
        public ICollection<BlocDeTexte> BlocsDeTexte { get; set; }
    }
}