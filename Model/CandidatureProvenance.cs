﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("CANDIDATURE_PROVENANCE")]
    public class CandidatureProvenance
    {
        [Key]
        [Column("CANDIDATURE_PROVENANCE_ID")]
        public int Id { get; set; }

        [Required]
        [Column("CANDIDATURE_PROVENANCE_INTITULE")]
        [MaxLength(50)]
        public string Intitule { get; set; }

        //[Column("CANDIDATURE_PROVENANCE_CANDIDATURES")]
        public ICollection<Candidature> Candidatures { get; set; }
    }
}
