﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("CANDIDATURE_EXPERIENCE")]
    public class CandidatureExperience
    {
        [Key]
        [Column("CANDIDATURE_EXPERIENCE_ID")]
        public int Id { get; set; }

        [Required]
        [Column("CANDIDATURE_EXPERIENCE_INTITULE")]
        [MaxLength(50)]
        public string Intitule { get; set; }

        //[Column("CANDIDATURE_EXPERIENCE_CANDIDATURES")]
        public ICollection<Candidature> Candidatures { get; set; }
    }
}
