﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    [Table("LigneAnnonce")]
    public class LigneAnnonce
    {
        [Key]
        [Column("LIGNE_ANNONCE_ID")]
        public int Id { get; set; }

        [Column("LIGNE_ANNONCE_ORDRE")]
        public int Ordre { get; set; }

        //propriété de navigation

        public Annonce Annonce { get; set; }

        public BlocDeTexte BlocDeTexte { get; set; }
    }
}
