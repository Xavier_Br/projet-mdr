﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Model
{
    [Table("ARBRENOEUD")]
    public class ArbreNoeud
    {
        [Key]
        [Column("ARBRENOEUD_ID")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("ARBRENOEUD_NOMDOSSIER")]
        public string NomDossier { get; set; }

        //Propriété de Navigation
        public ArbreNoeud NoeudParent { get; set; }
        public ICollection<ArbreNoeud> NoeudsEnfant { get; set; }
        public ICollection<Candidature> Candidature { get; set; }

    }
}