﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("CANDIDATURE")]
    public class Candidature
    {
        [Key]
        [Column("CANDIDATURE_ID")]
        public int Id { get; set; }

        [MaxLength(100)]
        [Column("CANDIDATURE_TECHNO")]
        public string Techno { get; set; }

        [Column("CANDIDATURE_COMMENTAIRE")]
        public string Commentaire { get; set; }

        [MaxLength(50)]
        [Column("CANDIDATURE_COOPTANT")]
        public string Cooptant { get; set; }

        [Column("CANDIDATURE_FLAGIMPORTANCE")]
        public bool FlagImportance { get; set; }

        [MaxLength(50)]
        [Column("CANDIDATURE_REFERENCE_ANNONCE")]
        public string ReferenceAnnonce
        { get; set; }

        public bool Lu { get; set; }



        // propriété de navigation    

        //[Column("CANDIDATURE_CANDIDAT")]
        public virtual Candidat Candidat { get; set; }

        //[Column("CANDIDATURE_POSTE")]
        public string Poste { get; set; }

        //[Column("CANDIDATURE_EXPERIENCE")]
        public string Experience { get; set; }

        //[Column("CANDIDAT_PROVENANCE")]
        public string Provenance { get; set; }

        //[Column("CANDIDATURE_STATUT")]
        public ICollection<CandidatureStatut> Statuts { get; set; }

        //[Column("ARBRENOEUD_DOSSIERPARENT")]
        public ArbreNoeud DossierParent { get; set; }


    }
}
