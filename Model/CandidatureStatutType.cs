﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    [Table("CANDIDATURE_STATUT_TYPE")]
    public class CandidatureStatutType
    {
        [Key]
        [Column("CANDIDATURE_STATUT_TYPE_ID")]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        [Column("CANDIDATURE_STATUT_TYPE_INTITULE")]
        public string Intitule { get; set; }

        //Propriété de Navigation
        public ICollection<Candidat> Candidatures { get; set; }
    }
}
