﻿using DAL;
using Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projet_mdr.Services
{
    public class ActionDictionnary
    {

        public ActionDictionnary()
        {
            Dictionary<string, Func<int[], bool>> Dictionnaire = new Dictionary<string, Func<int[], bool>>();
            Dictionnaire["Archiver"] = new Func<int[], bool>(Archiver);
            Dictionnaire["Valider la candidature"] = new Func<int[], bool>(Valider);
            Dictionnaire["Marquer comme non lu"] = new Func<int[], bool>(NonLu);
            Dictionnaire["Marquer comme lu"] = new Func<int[], bool>(MarquerLu);
            Dictionnaire["Marquer comme important"] = new Func<int[], bool>(Favoriser);
            Dictionnaire["Enlever importance"] = new Func<int[], bool>(deFavoriser);
            this.Dictionnaire = Dictionnaire;
        }

        public Dictionary<string, Func<int[], bool>> Dictionnaire { get; private set; }

        public bool Archiver(int[] idCandidatures)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int candidature in idCandidatures)
                {
                    Candidature objet = gdd.DepotCandidatures.RechercheCandidatureParId(candidature);
                    objet.Statuts.Add(new CandidatureStatut() { NomStatut = "archivé", ModifieLe = DateTime.Now, ModifiePar = "Admin" });
                }
                gdd.DepotCandidatures.PersistChanges();
            }

            return true;
        }

        public bool Valider(int[] idCandidatures)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int candidature in idCandidatures)
                {
                    Candidature objet = gdd.DepotCandidatures.RechercheCandidatureParId(candidature);
                    objet.Statuts.Add(new CandidatureStatut() { NomStatut = "retenu", ModifieLe = DateTime.Now, ModifiePar = "Admin" });

                }
                gdd.DepotCandidatures.PersistChanges();
            }

            return true;
        }

        public bool NonLu(int[] idCandidatures)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int candidature in idCandidatures)
                {
                    Candidature objet = gdd.DepotCandidatures.RechercheCandidatureParId(candidature);
                    objet.Lu = false;

                }
                gdd.DepotCandidatures.PersistChanges();
            }

            return true;
        }

        public bool MarquerLu(int[] idCandidatures)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int candidature in idCandidatures)
                {
                    Candidature objet = gdd.DepotCandidatures.RechercheCandidatureParId(candidature);
                    objet.Lu = true;
                }
                gdd.DepotCandidatures.PersistChanges();
            }
            return true;
        }

        public bool Favoriser(int[] idCandidatures)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int candidature in idCandidatures)
                {
                    Candidature objet = gdd.DepotCandidatures.RechercheCandidatureParId(candidature);
                    objet.FlagImportance = true;
                    gdd.DepotCandidatures.Update(objet);
                }
                gdd.DepotCandidatures.PersistChanges();
            }
            return true;
        }

        public bool deFavoriser(int[] idCandidatures)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int candidature in idCandidatures)
                {
                    Candidature cand = gdd.DepotCandidatures.RechercheCandidatureParId(candidature);
                    cand.FlagImportance = false;
                    gdd.DepotCandidatures.Update(cand);
                }
                gdd.DepotCandidatures.PersistChanges();
            }
            return true;
        }
    }
}