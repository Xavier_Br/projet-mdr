﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace projet_mdr
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

           // routes.MapRoute(
           //    name: "InsertionBDT",
           //    url: "InsertionBDT/Index/{id}",
           //    defaults: new { controller = "InsertionBDT", action = "Index", id = UrlParameter.Optional }
           //);

            //routes.MapRoute(
            //    name: "AnnonceEdition",
            //    url: "AnnonceEdition/Index/{id}",
            //    defaults: new { controller = "AnnonceEdition", action = "Index", id= UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "Annonces",
            //    url: "Annonces/Index/{*filters}",
            //    defaults: new { controller = "Annonces", action = "Index", filters = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    name: "MenuAnnonce",
            //    url: "MenuAnnonce/PublierAnnonce",
            //    defaults: new { controller = "MenuAnnonce", action = "PublierAnnonce" }
            //    );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Accueil", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
