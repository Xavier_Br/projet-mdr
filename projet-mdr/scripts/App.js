﻿////
// <General>
////

//


// Met en place un systeme de notification
function alerte(message, type) {
    var typeclass;
    var typeicon;
    switch (type) {
        case "erreur":
            typeclass = "alert-danger";
            typeicon = "glyphicon-remove";
            break;
        case "succes":
            typeclass = "alert-success";
            typeicon = "glyphicon-ok";
            break;
        case "attention":
            typeclass = "alert-warning";
            typeicon = "glyphicon-warning-sign";
            break;
        default:
            typeclass = "alert-info";
            typeicon = "glyphicon-info-sign";
    }
    var alertemsg = '<div class="alert ' + typeclass + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong><span class="glyphicon ' + typeicon + '"></span></strong> ' + message + '</div>';
    $(".alerts").append(alertemsg);

}

////
// <Annonces>
////

// Va à la page Annonces/Editer correspondant à l'annonce sélectionnée
function allerVersEdition() {
    var idannonceselect = $('#resultats').find('option:selected').val();
    if (idannonceselect == undefined) {
        alerte("Pas d'annonce sélectionnée", "erreur");
    }
    else {
        var url = 'Annonces/Editer' + '?id=' + idannonceselect;
        window.location.href = url;
    }
}

//Va ) la page Annonces/Apercu correspondant à l'annonce sélectionnée
function allerVersApercu() {
    var idannonceselect = $('#resultats').find('option:selected').val();
    if (idannonceselect == undefined) {
        alerte("Pas d'annonce sélectionnée", "erreur");
    }
    else {
        var url = 'Annonces/Apercu' + '?idAnnonce=' + idannonceselect;
        window.location.href = url;
    }
}

// Met à jour la vue partielle BlocDeTexte/Lister dans Annonces/Editer|Creer

function updatePartialView() {
    //empêche IE de mettre en cache le résultat de la requête
    $.ajaxSetup({ cache: false });
    var filtretext = $('#filtre').val();
    var intituleTypetext = $('#selectedType').find('option:selected').text();
    $("#ajaxloader").css("visibility", "visible");
    $.ajax({
        url: "/BlocDeTexte/Lister",
        type: "GET",
        data: { filtre: filtretext, intituletype: intituleTypetext }
    })
    .done(function (partialViewResult) {
        $("#listerContent").html(partialViewResult);
        $("#ajaxloader").css("visibility", "hidden");
    });
};

// Compte le nombre de bloc de texte supprimé par l'utilisateur pour ne pas rajouter un id existant sur un nouveau bloc de texte
var nombreBDTSupprime = 0; 

// Charge dans le DOM un bloc correspondant à un nouveau bloc de texte
function AjouterBDT() {
    var nombreBDT = (document.getElementById("blocBDT").childElementCount + 1 + nombreBDTSupprime);

    var bdtDiv = document.createElement("div");
    bdtDiv.id = "divId" + nombreBDT;

    var hiddenInput = document.createElement("input");
    hiddenInput.value = "-1";
    hiddenInput.id = "inputHidden" + nombreBDT;
    hiddenInput.name = "hiddenId";
    hiddenInput.type = "hidden";

    var bdt = document.createElement("textarea");
    bdt.placeholder = "Inserer votre texte ici";
    bdt.className = "form-control AnnonceEditionTxtArea";
    bdt.id = "texte" + nombreBDT;
    bdt.name = "texteBDT";

    var txtdiv = document.createElement("div");
    txtdiv.className = "col-md-8";


    var butdiv = document.createElement("div");
    butdiv.className = "col-md-4";

    var bdtBouttonSauvegarder = document.createElement("input");
    bdtBouttonSauvegarder.className = "btn btn-default";
    bdtBouttonSauvegarder.value = "Sauvegarder";
    bdtBouttonSauvegarder.type = "button";
    bdtBouttonSauvegarder.onclick = function () { EnregistrerBDT(nombreBDT); };


    var bdtBouttonInserer = document.createElement("input");
    bdtBouttonInserer.className = "btn btn-default";
    bdtBouttonInserer.value = "Inserer";
    bdtBouttonInserer.type = "button";
    bdtBouttonInserer.onclick = function () { chargerBDT(nombreBDT) };

    var bdtBouttonSupprimer = document.createElement("input");
    bdtBouttonSupprimer.value = "x";
    bdtBouttonSupprimer.type = "button";
    bdtBouttonSupprimer.className = "btn btn-default";
    bdtBouttonSupprimer.onclick = function () { SupprimerBDT(nombreBDT); };

    bdtDiv.appendChild(hiddenInput);

    txtdiv.appendChild(bdt);
    bdtDiv.appendChild(txtdiv);

    butdiv.appendChild(bdtBouttonSauvegarder);
    butdiv.appendChild(bdtBouttonInserer);
    butdiv.appendChild(bdtBouttonSupprimer);

    bdtDiv.appendChild(butdiv);
    document.getElementById("blocBDT").appendChild(bdtDiv);
}

// Supprimer dans le DOM le bloc de texte passé en argument
function SupprimerBDT(idBDT) {
    bdtASupprimer = document.getElementById("divId" + idBDT);

    bdtASupprimer.parentNode.removeChild(bdtASupprimer);

    nombreBDTSupprime++;
}

// Ouvre le modal d'insertion de Bloc de texte
function chargerBDT(index) {
    $("#renderInsertionPartial").modal();
    $("#currentBdt").text(index);
}

// Ouvre le modal de création de Bloc de texte
function EnregistrerBDT(idBDT) {
    document.getElementById("currentBdt").value = idBDT;
    document.getElementById("texteBDT").value = document.getElementById("texte" + idBDT).value;
    $("#renderEnregistrerPartial").modal();
}

////
// <Bloc De Texte>
////

// Requête AJAX pour récupèrer le contenu d'un Bloc de texte
function getBDTTxt() {
    var idbloc = $("#listeBDT").find('option:selected').val();
    $.ajax({
        type: "GET",
        data: { id: idbloc },
        url: "/BlocDeTexte/RecupererTexte",
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            if (response != null) {
                $("#ContenuBDT").html(response.replace(/(?:\r\n|\r|\n)/g, '<br />'));
            } else {
                 alerte("erreur de chargement");
            }
        },
        error: function (response) {
            alerte("erreur!");
        }

    });
}

// Insere le bloc sélectionné dans l'annonce en cours 
function insererBDT() {
    var index = $("#currentBdt").text();
    var textarea = document.querySelector("#texte" + index)
    textarea.value = $("#ContenuBDT").html().replace(/<br\s*\/?>/mg, "\n");
    textarea.style.height = "5px";
    textarea.style.height = (textarea.scrollHeight) + "px";
    alerte("Insertion réussie");
}

// Requête AJAX de création d'un nouveau bloc de texte
function SauvegarderBDT() {
    var indexBdt = $("#currentBdt").val();

    var idtype = $("#typeselect").find('option:selected').val();
    var titre = $("#titreBDT").val();
    var contenu = $("#texteBDT").val();

    if (idtype == "") {
        alerte("Le champ 'Type de bloc' doit être renseigné", "erreur");
        return;
    }
    if (contenu == "") {
        alerte("Le contenu ne peut pas être vide", "erreur");
        return;
    }
    if (titre == "") {
        alerte("Le titre doit être renseigné", "erreur")
        return;
    }
    $("#ajaxloader").css("visibility", "visible");
    $.ajax({
        type: "POST",
        data: JSON.stringify({ id: idtype, titrebdt: titre, nouveautextebdt: contenu }),
        url: "/BlocDeTexte/Enregistrer",
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            if (response != null) {
                $("#inputHidden" + indexBdt).val(response);
                $("#texte" + indexBdt).val(contenu);
                alerte("Sauvegarde réussie", "succes")
            } else {
                alerte("erreur", "erreur");
            }
            $("#ajaxloader").css("visibility", "hidden");
        },
        error: function (response) {
            alerte("erreur!", "erreur");
            $("#ajaxloader").css("visibility", "hidden");
        }
    });
}

// Requête AJAX pour créer un nouveau type de bloc de texte
function addBdtType() {
    var nouveaunom = prompt("Intitule du nouveau type");
    if (nouveaunom == null || nouveaunom.length == 0) {
        alerte("l'intitule ne peut pas être vide", "erreur");
    }
    else{
    $.ajax({
        type: "POST",
        data: JSON.stringify({ nom:nouveaunom }),
        url: "/BlocDeTexte/CreerBDTType",
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            if (response != null) {
                updatePartialView();
                alerte("Création réussie", "succes")
            } else {
                alerte("erreur", "erreur");
            }
        },
        error: function (response) {
            alerte("erreur!", "erreur");
        }
    });
    }
}

// Requête AJAX pour archiver un Bloc De Texte
function archiverBdt() {
    var idbloc = $("#listeBDT").find('option:selected').val();
    if (!confirm("Attention ce bloc ne vous sera plus jamais proposé, confirmez-vous?")) {
        return false
    }
    $("#ajaxloader").css("visibility", "visible");
    $.ajax({
        type: "POST",
        data: JSON.stringify({ id: idbloc }),
        url: "/BlocDeTexte/Archiver",
        contentType: 'application/json; charset=utf-8',
        success: function (response) {
            if (response != null) {
                updatePartialView();
                alerte("Archivage réussi", "succes")
            } else {
                alerte("erreur", "erreur");
            }
            $("#ajaxloader").css("visibility", "hidden");
        },
        error: function (response) {
            alerte("erreur!", "erreur");
            $("#ajaxloader").css("visibility", "hidden");
        }
    });

}









//////////////////:::





$(document).ready(function () {

    $("#Valider").click(function () {
        var selectedIDs = new Array();
        $('input:checkbox.checkBox').each(function () {
            if ($(this).prop('checked')) {
                selectedIDs.push($(this).val());
            }
        });

        var selector = $("#Select1").find("option:selected").val();
        var options = {};
        options.url = "/Candidature/actionSelection";
        options.type = "POST";
        options.data = JSON.stringify({ "candidatures": selectedIDs, "key": selector });
        options.contentType = "application/json";
        options.dataType = "json";
        options.success = function (msg) {
            alerte(msg);
            if (msg == "Les changements ont été effectués correctement") {
                window.location.href = window.location.href;
            }
            $("#ajaxloader").css("visibility", "hidden");
        };

        options.error = function () {
            alerte("Error");
            $("#ajaxloader").css("visibility", "hidden");
        };
        $("#ajaxloader").css("visibility", "visible");
        $.ajax(options);
    });
});

function VerifCandidatExist() {
    var nom = $("#Nom").val();
    var prenom = $("#Prenom").val();
    var dateNaissance = $("#DateDeNaissance").val();
    var email = $("#Email").val();

    if (nom == "" || prenom == "" || dateNaissance == "" || email == "" || !$('form')[0].checkValidity()) {
        alerte("Veuillez saisir les champs nom, prénom, date de naissance et e-mail");
        return false;
    }
    $("#ajaxloader").css("visibility", "visible");
    $.ajax({
        type: "POST",
        data: JSON.stringify({ nom: nom, prenom: prenom, dateNaissance: dateNaissance }),
        url: "/Candidat/VerifCandidatExist",
        contentType: "application/json; charset=utf-8",
        success: function (idCandidat) {
            if (idCandidat > 0) {
                var candidat = confirm("Il existe déjà un candidat. Souhaitez-vous l'afficher ?");
                if (candidat == true) {
                    window.location.href = "/Candidat/Details/" + idCandidat;
                }
                else {
                    $("form").submit();
                }
            }
            else {
                $("form").submit();
            }
        },
        error: function () {
            alerte("erreur vérification impossible car un champ n'est pas correct", "erreur");
            $("#ajaxloader").css("visibility", "hidden");
        }
    });


}

// Gestion de l'apparence customisée pour le parcours de fichier (upload)

$(document).on('change', ':file', function () {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).ready(function () {
    $(':file').on('fileselect', function (event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if (input.length) {
            input.val(log);
        } else {
            if (log) alert(log);
        }

    });
});

function FiltrerCandidatures(statut, e) {
    var sender = (e && e.target) || (window.event && window.event.srcElement);
    sender.classList.toggle("font-bold");

    $("[name=ligne]").has("td:contains('" + statut + "')").toggle();
}

function changerstatut(statut, id) {
    $("#ajaxloader").css("visibility", "visible");
    $.ajax({
        type: "POST",
        data: JSON.stringify({ statut: statut, id: id}),
        url: "/Candidature/ChangerStatut",
        contentType: "application/json; charset=utf-8",
        success: function (reponse) {
            alerte(reponse, "succes")
            $("#ajaxloader").css("visibility", "hidden");
        },
        error: function () {
            $("#ajaxloader").css("visibility", "hidden");
        }
    });


}

function changerstatutselectionne(id) {
    var statut = $('#statutselect').find('option:selected').val();
    changerstatut(statut, id)
}

function allerversmail() {
    var urlbase="/Candidature/EnvoiMail?";
    $('input:checkbox.checkBox').each(function () {
        if ($(this).prop('checked')) {
            urlbase = urlbase + "ids=" + $(this).val()+"&";
        }
    });
    urlbase = urlbase.substring(0, urlbase.length - 1)
    if (urlbase != "/Candidature/EnvoiMail") {
        window.location.href = urlbase;
    }
    else {
        alerte("Aucune candidature sélectionnée","erreur")
    }
}

function allerversrevue() {
    var urlbase = "/Candidature/Revue?";
    $('input:checkbox.checkBox').each(function () {
        if ($(this).prop('checked')) {
            urlbase = urlbase + "ids=" + $(this).val() + "&";
        }
    });
    urlbase = urlbase.substring(0, urlbase.length - 1)
    if (urlbase != "/Candidature/Revue") {
        window.location.href = urlbase;
    }
    else {
        alerte("Aucune candidature sélectionnée", "erreur")
    }
}

function generermails() {
    var mailid = $('#selectmail').val();
    if (mailid == "") {
        alerte("Pas de template sélectionné", "erreur");
        return false;
    }

    var mailbody = $("#MailBody" + mailid).html().replace(/<br\s*\/?>/mg, "%0D%0A");
    var mailsubject = $("#MailSubject" + mailid).html();
    

    $('input:checkbox.checkBox').each(function () {
        if ($(this).prop('checked')) {
            var mailtoHref = "mailto:" + $(this).val() + "?subject=" + mailsubject + "&body=" + mailbody;
            $('body').append('<iframe id="mailtoHack" src="' + mailtoHref + '"/>');
            $('#mailtoHack').remove();
            
        }
    });
}

function toggleImportance(id,e) {

    var sender = (e && e.target) || (window.event && window.event.srcElement);

    var options = {};
    options.url = "/Candidature/changerImportance";
    options.type = "POST";
    options.data = JSON.stringify({ "id": id });
    options.contentType = "application/json";
    options.dataType = "json";
    options.success = function () {
        sender.classList.toggle("glyphicon-star");
        sender.classList.toggle("glyphicon-star-empty");
        sender.classList.toggle("hover-visible");
        $("#ajaxloader").css("visibility", "hidden");
    };
    options.error = function () {
        alerte("Error");
        $("#ajaxloader").css("visibility", "hidden");
    };
    $("#ajaxloader").css("visibility", "visible");
    $.ajax(options);
}

function quickSearch() {
    var motcle = $("#quickSearch").val();
    window.location.href = "/Candidature/Index?quickSearch=" + motcle;
}

$(document).ready(function () {
    $("#listeCandidature").tablesorter({ headers: { 0: { sorter: false } } });
}
);

function UpdateQueryString(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}


function changeCandidature(change) {
    var newIndex = parseInt($("#index").val()) + change;
    window.location.href=UpdateQueryString("index", newIndex, window.location.href)
}

function nextCandidature() {
    changeCandidature(1);
}

function prevCandidature() {
    changeCandidature(-1);
}


$(document).ready(function () {
    $('#selectmail').change(function () {
        var mailid = "#mail"+$(this).val();
        $(".maildiv").not(mailid).hide();
        $(mailid).show();
    });
});
function changeCheck() {
    $('input:checkbox.checkBox:visible').each(function () {
        $(this).prop('checked', $("#globalcheck").prop('checked'));
    });
}

function verifandsave(type) {
    var texte = $("#" + type + "Intitule").val();
    if (texte == null || texte.length == 0 || texte.length > 50) {
        alerte("l'intitulé est obligatoire et doit faire moins de 50 caractères");
    }
    else {
        document.getElementById(type).submit();
    }
}