﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Web.Mvc;

namespace projet_mdr.ViewModels
{
    public class InsertionBDTVueModele
    {
        public IEnumerable<SelectListItem> ListeTypeBDT;
        public IEnumerable<SelectListItem> ListeBDT;
        public string filtre;
    }
}