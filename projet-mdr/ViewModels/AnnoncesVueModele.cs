﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model;
using System.Web.Mvc;

namespace projet_mdr.ViewModels
{
    public class AnnoncesVueModele
    {
        public IEnumerable<SelectListItem> ListeAnnonces;
        public IEnumerable<SelectListItem> ListeCreePar;
    }
}