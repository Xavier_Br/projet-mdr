﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using projet_mdr.ViewModels;
using DAL;
using DAL.Depots;
using Extensions;

namespace projet_mdr.Controllers
{
    public class AnnoncesController : Controller
    {
        // GET: Annonces
        public ActionResult Index(string filters, string selectauteur)
        {
            AnnoncesVueModele AVM = new AnnoncesVueModele();
            List<Annonce> ListAnnonces;
            List<string> ListCreePar;

            // Requêtage BDD
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                ListAnnonces = (String.IsNullOrWhiteSpace(filters) && String.IsNullOrWhiteSpace(selectauteur)) ?
                    gdd.DepotAnnonces.RechercherCinqDernieresAnnonces()
                    : gdd.DepotAnnonces.RechercheAnnonceParTitreouRefouAuteur(selectauteur, filters);

                ListCreePar = gdd.DepotAnnonces.RecupereTousLesAuteurs();
            }

            // Creation des listes d'objets à afficher
            AVM.ListeAnnonces = ListAnnonces.Select(l => new SelectListItem { Value = l.Id.ToString(), Text = l.Nom });
            AVM.ListeCreePar = ListCreePar.Select(l => new SelectListItem { Text = l.ToString() });

            return View(AVM);
        }

        public ActionResult Apercu(int idAnnonce)
        {
            Annonce annonceAPublier = new Annonce();

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                annonceAPublier = gdd.DepotAnnonces.RecupererAnnonceParId(idAnnonce);
            }

            return View(annonceAPublier);
        }

        public ActionResult Creer()
        {
            return View();
        }

        public ActionResult Publier(int idAnnonce)
        {
            Annonce annonceAPublier;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                annonceAPublier = gdd.DepotAnnonces.RecupererAnnonceParId(idAnnonce);
                annonceAPublier.Statut.Intitule = "Publié";

                gdd.DepotAnnonces.Update(annonceAPublier);

                gdd.DepotAnnonces.PersistChanges();
            }

            this.AddNotification("Le statut de votre annonce est bien passé en 'publiée'", NotificationType.SUCCESS);

            return View("Apercu", annonceAPublier);
        }

        public ActionResult Editer(int? id)
        {
            Annonce AnnonceAEditer;

            if (id.HasValue == false)
            {
                return RedirectToAction("Creer");
            }
            else
            {
                using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
                {
                    AnnonceAEditer = gdd.DepotAnnonces.RecupererAnnonceParId(id.Value);
                }
            }

            if (AnnonceAEditer.Statut != null && AnnonceAEditer.Statut.Intitule == "Publié")
                this.AddNotification("Attention, vous êtes en train de modifier une annonce déjà publiée. L'enregistrer en créera une nouvelle", NotificationType.WARNING);

            return View(AnnonceAEditer);
        }

        [HttpPost]
        public ActionResult Enregistrer(List<string> texteBDT, string titreAnnonce, List<int> hiddenId, int? annonceId)
        {
            Annonce AnnonceASauvegarder;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                if (annonceId.HasValue && annonceId.Value > 0 && gdd.DepotAnnonces.RecupererAnnonceParId(annonceId.Value).Statut.Intitule != "Publié")
                {
                    AnnonceASauvegarder = gdd.DepotAnnonces.RecupererAnnonceParId(annonceId.Value);
                }
                else
                {
                    AnnonceASauvegarder = new Annonce();
                    AnnonceASauvegarder.Statut = new AnnonceStatut() { Intitule = "En création" };
                }

                AnnonceASauvegarder.Titre = titreAnnonce;
                AnnonceASauvegarder.Nom = titreAnnonce + " " + DateTime.Now.ToShortDateString();

                List<LigneAnnonce> listeLigneAnnonce = new List<LigneAnnonce>();

                for (int i = 0; i < texteBDT.Count(); i++)
                {
                    LigneAnnonce la = new LigneAnnonce();
                    BlocDeTexte bdt;

                    if (hiddenId[i] > 0 && gdd.DepotBlocDeTexte.RechercherParId(hiddenId[i]).Texte == texteBDT[i])
                    {
                        bdt = gdd.DepotBlocDeTexte.RechercherParId(hiddenId[i]);
                    }
                    else
                    {
                        bdt = new BlocDeTexte();
                        bdt.Nom = "Interne";
                        bdt.Texte = texteBDT[i];
                        bdt.NbAcces = 0;
                        bdt.DernierAccesLe = DateTime.Now;
                        bdt.ModifieLe = DateTime.Now;
                        bdt.ModifiePar = "Jean-Mi";
                        bdt.CreeLe = DateTime.Now;
                        bdt.CreePar = "Jean-Mi";
                    }

                    la.BlocDeTexte = bdt;
                    la.Ordre = i + 1;

                    listeLigneAnnonce.Add(la);
                }

                AnnonceASauvegarder.LignesAnnonce = listeLigneAnnonce;

                if (AnnonceASauvegarder.Statut.Intitule == "En création")
                {
                    gdd.DepotAnnonces.Add(AnnonceASauvegarder);
                    AnnonceASauvegarder.Statut.Intitule = "Créé";
                }
                else
                {
                    gdd.DepotAnnonces.Update(AnnonceASauvegarder);
                }
                gdd.DepotAnnonces.PersistChanges();
            }
            int idAnnonce = AnnonceASauvegarder.Id;
            this.AddNotification("Annonce sauvegardée", NotificationType.SUCCESS);
            return RedirectToAction("Apercu", "Annonces", new { idAnnonce = idAnnonce });
        }
    }
}