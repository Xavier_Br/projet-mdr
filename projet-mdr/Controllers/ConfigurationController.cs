﻿using DAL;
using Model;
using projet_mdr.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projet_mdr.Controllers
{
    public class ConfigurationController : Controller
    {
        // GET: Configuration
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MailTemplateConf()
        {
            List<MailTemplate> _listMailTemplates;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                _listMailTemplates = gdd.DepotMailTemplates.ListerTemplates();
            }

            ViewBag.ListeTemplates = _listMailTemplates;
            ViewBag.SelectListeTemplates = _listMailTemplates.Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Nom }).ToList();

            return View();
        }

        public ActionResult ComboCandidatureConf()
        {

            List<CandidatureExperience> listeXP;
            List<CandidatureProvenance> listePro;
            List<CandidaturePoste> listePoste;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                listeXP = gdd.DepotCandidatureExperience.ListerExperiences();
                listePro = gdd.DepotCandidatureProvenance.ListerProvenances();
                listePoste = gdd.DepotCandidaturePoste.ListerPostes();
            }

            ViewBag.ListePoste = listePoste;
            ViewBag.ListeExperience = listeXP;
            ViewBag.ListeProvenance = listePro;

            return View();
        }

        public ActionResult SupprimerExperience(int id)
        {

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidatureExperience.Delete(gdd.DepotCandidatureExperience.ExperienceParId(id));
                gdd.DepotCandidatureExperience.PersistChanges();
            }

            return RedirectToAction("ComboCandidatureConf");
        }

        public ActionResult AjouterExperience(CandidatureExperience experience)
        {

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidatureExperience.Add(experience);
                gdd.DepotCandidatureExperience.PersistChanges();
            }

            return RedirectToAction("ComboCandidatureConf");

        }

        public ActionResult SupprimerPoste(int id)
        {

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidaturePoste.Delete(gdd.DepotCandidaturePoste.PosteParId(id));
                gdd.DepotCandidaturePoste.PersistChanges();
            }

            return RedirectToAction("ComboCandidatureConf");
        }

        public ActionResult AjouterPoste(CandidaturePoste poste)
        {

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidaturePoste.Add(poste);
                gdd.DepotCandidaturePoste.PersistChanges();
            }

            return RedirectToAction("ComboCandidatureConf");

        }

        public ActionResult SupprimerProvenance(int id)
        {

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidatureProvenance.Delete(gdd.DepotCandidatureProvenance.ProvenanceParId(id));
                gdd.DepotCandidatureProvenance.PersistChanges();
            }

            return RedirectToAction("ComboCandidatureConf");
        }

        public ActionResult AjouterProvenance(CandidatureProvenance provenance)
        {
  
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidatureProvenance.Add(provenance);
                gdd.DepotCandidatureProvenance.PersistChanges();
            }

            return RedirectToAction("ComboCandidatureConf");

        }

        public ActionResult SupprimerMailTemplate(int id)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotMailTemplates.Delete(gdd.DepotMailTemplates.MailTemplateParId(id));
                gdd.DepotMailTemplates.PersistChanges();
            }

            return RedirectToAction("MailTemplateConf");
        }

        public ActionResult AjouterMailTemplate(MailTemplate template)
        {

            if (!ModelState.IsValid)
            {
                List<MailTemplate> _listMailTemplates;

                using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
                {
                    _listMailTemplates = gdd.DepotMailTemplates.ListerTemplates();
                }

                ViewBag.ListeTemplates = _listMailTemplates;
                ViewBag.SelectListeTemplates = _listMailTemplates.Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Nom }).ToList();

                return View("MailTemplateConf",template);
            }

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotMailTemplates.Add(template);
                gdd.DepotMailTemplates.PersistChanges();
            }

            return RedirectToAction("MailTemplateConf");

        }
    }
}