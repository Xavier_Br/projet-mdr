﻿using DAL;
using Extensions;
using Model;
using projet_mdr.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projet_mdr.Controllers
{
    public class CandidatureController : Controller
    {
        // GET: Candidature
        public ActionResult Index(string nom, string prenom, string poste,
                                  string techno, string experience, string provenance, string refAnnonce, string quickSearch,string archive)
        {
            List<Candidature> _listCandidature;
            List<CandidatureExperience> listeXP;
            List<CandidatureProvenance> listePro;
            List<CandidaturePoste> listePoste;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                if (!String.IsNullOrWhiteSpace(quickSearch))
                    _listCandidature = gdd.DepotCandidatures.RechercheSimple(quickSearch);
                else
                    _listCandidature = gdd.DepotCandidatures.RechercheToutArguments(nom ?? "", prenom ?? "", poste ?? "", techno ?? "", experience ?? "", provenance ?? "", refAnnonce ?? "");

                listeXP = gdd.DepotCandidatureExperience.ListerExperiences();
                listePro = gdd.DepotCandidatureProvenance.ListerProvenances();
                listePoste = gdd.DepotCandidaturePoste.ListerPostes();
            }

            ActionDictionnary dico = new ActionDictionnary();
            ViewBag.ActionList = dico.Dictionnaire.Select(l => new SelectListItem { Value = l.Key, Text = l.Key }).ToList();
            ViewBag.ListePoste = listePoste.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();
            ViewBag.ListeExperience = listeXP.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();
            ViewBag.ListeProvenance = listePro.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();

            if(String.IsNullOrWhiteSpace(archive) || archive !="avec")
                _listCandidature = _listCandidature.Where(c=>c.Statuts.Last().NomStatut!="archivé").ToList();


            if (_listCandidature.Count > 50)
            {
                _listCandidature = _listCandidature.Take(50).ToList();
                this.AddNotification("il y a plus de 50 candidatures correspondantes, seules les 50 plus récentes ont été affichées", NotificationType.WARNING);
            }

            List<SelectListItem> listeStatuts = _listCandidature.GroupBy(c => c.Statuts.Last().NomStatut, c => c.Statuts.Last().NomStatut, (key, g) => new SelectListItem
            {
                Text = key,
                Value = g.ToList().Count.ToString()
            }).ToList();

            ViewBag.listeStatuts = listeStatuts;



            return View(_listCandidature);
        }

        public ActionResult actionSelection(int[] candidatures, string key)
        {
            if (candidatures != null)
            {

                ActionDictionnary Dico = new ActionDictionnary();
                bool resultat = Dico.Dictionnaire[key](candidatures);
                if (resultat == true)
                {
                    return Json("Les changements ont été effectués correctement");
                }
                return Json("Un problème a eu lieu lors de l'action demandée");
            }

            else return Json("Aucune candidature sélectionnée");
        }

        [HttpPost]
        public ActionResult Enregistrer(Candidature nouvelleCandidature, int Idcandidat)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                Candidat candidat = gdd.DepotCandidats.RechercherCandidatParId(Idcandidat);
                nouvelleCandidature.Candidat = candidat;

                gdd.DepotCandidatures.Add(nouvelleCandidature);
                gdd.DepotCandidatures.PersistChanges();

            }
            return RedirectToAction("Index", "Candidature");

        }

        public ActionResult Details(int id)
        {
            Candidature candidature;
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidature = gdd.DepotCandidatures.RechercheCandidatureParId(id);
                candidature.Lu = true;
                gdd.DepotCandidatures.Update(candidature);
                gdd.DepotCandidatures.PersistChanges();

                Candidat candidat = gdd.DepotCandidats.RechercherCandidatParId(candidature.Candidat.Id);
                candidature.Candidat = candidat;
            }

            ViewBag.returnUrl = Request.UrlReferrer;

            return View(candidature);
        }

        public ActionResult Créer(int id)
        {
            Candidat candidat;

            List<CandidatureExperience> listeXP;
            List<CandidatureProvenance> listePro;
            List<CandidaturePoste> listePoste;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidat = gdd.DepotCandidats.RechercherCandidatParId(id);
                listeXP = gdd.DepotCandidatureExperience.ListerExperiences();
                listePro = gdd.DepotCandidatureProvenance.ListerProvenances();
                listePoste = gdd.DepotCandidaturePoste.ListerPostes();
            }

            ViewBag.ListePoste = listePoste.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();
            ViewBag.ListeExperience = listeXP.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();
            ViewBag.ListeProvenance = listePro.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();

            return View(candidat);
        }

        public ActionResult Editer(int id)
        {
            Candidature candidature;

            List<CandidatureExperience> listeXP;
            List<CandidatureProvenance> listePro;
            List<CandidaturePoste> listePoste;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidature = gdd.DepotCandidatures.RechercheCandidatureParId(id);
                listeXP = gdd.DepotCandidatureExperience.ListerExperiences();
                listePro = gdd.DepotCandidatureProvenance.ListerProvenances();
                listePoste = gdd.DepotCandidaturePoste.ListerPostes();

                Candidat candidat = gdd.DepotCandidats.RechercherCandidatParId(candidature.Candidat.Id);
                candidature.Candidat = candidat;
            }

            ViewBag.ListePoste = listePoste.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();
            ViewBag.ListeExperience = listeXP.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();
            ViewBag.ListeProvenance = listePro.Select(l => new SelectListItem { Value = l.Intitule, Text = l.Intitule }).ToList();

            ViewBag.returnUrl = Request.UrlReferrer;

            return View(candidature);
        }

        [HttpPost]
        public ActionResult UploadCV(HttpPostedFileBase file, int idcandidat, string returnUrl)
        {
            Candidat candidat;
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {

                candidat = gdd.DepotCandidats.RechercherCandidatParId(idcandidat);

                string nompdf = @"TestPdf\" + candidat.Id.ToString() + "_" + candidat.Nom + "_" + candidat.Prenom + ".pdf";

                if (file != null && file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath(@"~\"), nompdf);

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        file.SaveAs(path);
                    }
                    else file.SaveAs(path);

                    //candidat.ModifiePar = modifiepar;
                    candidat.CV = nompdf;
                    gdd.DepotCandidats.Update(candidat);
                    gdd.DepotCandidats.PersistChanges();
                }

                //return RedirectToAction("Créer",new { id = idcandidat });
                TempData["returnUrl"] = returnUrl;
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        [HttpPost]
        public ActionResult MettreAJour(Candidature candidatureAMAJ, int Idcandidat)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                Candidature candidaturebdd = gdd.DepotCandidatures.RechercheCandidatureParId(candidatureAMAJ.Id);

                candidaturebdd.Experience = candidatureAMAJ.Experience;
                candidaturebdd.Poste = candidatureAMAJ.Poste;
                candidaturebdd.Commentaire = candidatureAMAJ.Commentaire;
                candidaturebdd.Cooptant = candidatureAMAJ.Cooptant;
                candidaturebdd.Provenance = candidatureAMAJ.Provenance;
                candidaturebdd.Techno = candidatureAMAJ.Techno;
                candidaturebdd.ReferenceAnnonce = candidatureAMAJ.ReferenceAnnonce;

                gdd.DepotCandidatures.Update(candidaturebdd);
                gdd.DepotCandidatures.PersistChanges();

            }
            return RedirectToAction("Details", "Candidature", new { id = candidatureAMAJ.Id });

        }

        public ActionResult ChangerStatut(string statut, int id)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                Candidature candidature = gdd.DepotCandidatures.RechercheCandidatureParId(id);
                candidature.Statuts.Add(new CandidatureStatut() { NomStatut = statut, ModifieLe = DateTime.Now, ModifiePar = "admin" });
                gdd.DepotCandidatures.Update(candidature);
                gdd.DepotCandidatures.PersistChanges();
            }

            switch (statut)
            {
                case "retenu":
                    return Content("La candidature a bien été validée");
                case "refusé":
                    return Content("La candidature a bien été refusée");
                case "à réétudier":
                    return Content("La candidature a bien été mis à réétudier");
                case "archivé":
                    return Content("La candidature a bien été archivée");
            }

            return Content("changement réussi");
        }

        public ActionResult EnvoiMail(int[] ids)
        {
            List<Candidature> _listCandidature = new List<Candidature>();
            List<MailTemplate> _listMailTemplates;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                foreach (int id in ids)
                {
                    _listCandidature.Add(gdd.DepotCandidatures.RechercheCandidatureParId(id));     
                }
                _listMailTemplates = gdd.DepotMailTemplates.ListerTemplates();
            }

            ViewBag.ListeTemplates = _listMailTemplates;
            ViewBag.SelectListeTemplates = _listMailTemplates.Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Nom }).ToList();
            return View(_listCandidature);
        }

        public ActionResult changerImportance(int id)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                Candidature candidature = gdd.DepotCandidatures.RechercheCandidatureParId(id);
                candidature.FlagImportance = !candidature.FlagImportance;
                gdd.DepotCandidatures.PersistChanges();
            }

            return Json("réussi");
        }


        public ActionResult Revue(int[] ids,int? index)
        {
            if (!index.HasValue ||index.Value<0 )
            {
                index = 0;
            }
            else if(index.Value > ids.Count()-1){
                index = ids.Count()-1;
            }

            Candidature candidature;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidature = gdd.DepotCandidatures.RechercheCandidatureParId(ids[index.Value]);
            }

            ViewBag.currentIndex = index.Value;
            ViewBag.itemCount = ids.Count();
            return View(candidature);
        }
    }
}
