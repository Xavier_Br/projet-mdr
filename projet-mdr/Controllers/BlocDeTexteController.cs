﻿using DAL;
using DAL.Depots;
using Model;
using projet_mdr.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projet_mdr.Controllers
{
    public class BlocDeTexteController : Controller
    {
        public ActionResult Lister(string filtre, string intituletype)
        {
            InsertionBDTVueModele IVM = new InsertionBDTVueModele();
            List<BlocDeTexte> ListeBDT;
            List<TypeBDT> ListeType;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                ListeBDT = gdd.DepotBlocDeTexte.RechercherBDTParTypeEtFiltre(intituletype, filtre);
                ListeType = gdd.DepotBDTType.ListerTypes();
            }

            IVM.ListeBDT = ListeBDT.Select(l => new SelectListItem { Value = l.Id.ToString(), Text = l.Nom.ToString() });
            IVM.ListeTypeBDT = ListeType.Select(t => new SelectListItem { Value = t.Id.ToString(), Text = t.Intitule.ToString(), Selected = (t.Intitule == intituletype) });
            IVM.filtre = filtre;

            return PartialView(IVM);
        }

        public ActionResult RecupererTexte(int? id)
        {

            if (!id.HasValue)
                return Content("");

            string content;
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                content = gdd.DepotBlocDeTexte.RechercherParId(id.Value).Texte;
            }

            return Content(content);

        }

        public ActionResult Creer(string texte1)
        {
            List<TypeBDT> ListeType;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                ListeType = gdd.DepotBDTType.ListerTypes();
            }

            ViewBag.ListeType = ListeType.Select(t => new SelectListItem { Value = t.Id.ToString(), Text = t.Intitule }).ToList();
            return PartialView();
        }

        public ActionResult Enregistrer(string titrebdt, string nouveautextebdt, int id)
        {
            int idBdt;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                BlocDeTexte bdtsauvegarde = new BlocDeTexte();
                bdtsauvegarde.Nom = titrebdt;
                bdtsauvegarde.Texte = nouveautextebdt;
                bdtsauvegarde.DernierAccesLe = DateTime.Now;
                bdtsauvegarde.Type = gdd.DepotBDTType.RecupererTypeParId(id);

                gdd.DepotBlocDeTexte.Add(bdtsauvegarde);
                gdd.DepotBlocDeTexte.PersistChanges();

                idBdt = bdtsauvegarde.Id;
            }

            return Content(idBdt.ToString());

        }

        public ActionResult CreerBdtType(string nom)
        {
            if (String.IsNullOrEmpty(nom))
                return Content("");

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotBDTType.Add(new TypeBDT() {Intitule=nom});
                gdd.DepotBDTType.PersistChanges();
            }

            return Content("insertion réussie");
        }

        public ActionResult Archiver(int id)
        {
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                BlocDeTexte bdt = gdd.DepotBlocDeTexte.RechercherParId(id);
                bdt.Type = null;
                gdd.DepotBlocDeTexte.Update(bdt);
                gdd.DepotBlocDeTexte.PersistChanges();
            }

            return Content("réussi");
        }

    }
}