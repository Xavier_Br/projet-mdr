﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projet_mdr.Controllers
{
    public class CandidatController : Controller
    {
        // GET: Candidat
        public ActionResult Index()
        {
            List<Candidat> candidats;
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidats = gdd.DepotCandidats.ListerTousLesCandidats();
            }

            return View(candidats);
        }

        public ActionResult Details(int id)
        {
            Candidat candidat;
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidat = gdd.DepotCandidats.RechercherCandidatParId(id);
            }
            return View(candidat);
        }

        public ActionResult Créer()
        {
            return View();
        }

        public ActionResult Editer(int id)
        {
            Candidat candidat;
            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidat = gdd.DepotCandidats.CandidatParId(id);
            }
            ViewBag.returnUrl = Request.UrlReferrer.ToString();
            return View(candidat);
            
        }


        //
        [HttpPost]
        public ActionResult Enregistrer(Candidat nouveauCandidat)
        {
            if (!ModelState.IsValid)
                return View("Créer",nouveauCandidat);

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                gdd.DepotCandidats.Add(nouveauCandidat);
                gdd.DepotCandidats.PersistChanges();
            }
            return RedirectToAction("Details", "Candidat", new { id = nouveauCandidat.Id });
        }

        public ActionResult VerifCandidatExist(string nom, string prenom, DateTime dateNaissance)
        {
            List<Candidat> candidat = new List<Candidat>();
            string content;

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                candidat = gdd.DepotCandidats.CandidatsSimilaire(nom, prenom, dateNaissance);
                if (candidat.Count > 0)
                {
                    int id = candidat.First().Id;
                    Candidat candidatExist = gdd.DepotCandidats.RechercherCandidatParId(id);
                    content = id.ToString();
                }
                else
                    content = "-1";
            }
            return Content(content);
        }

        [HttpPost]
        public ActionResult MettreAJour(Candidat candidat, string returnUrl)
        {

            if (!ModelState.IsValid)
                return View("Editer",candidat);

            using (GestionnaireDeDepot gdd = new GestionnaireDeDepot())
            {
                Candidat candidatbdd = gdd.DepotCandidats.RechercherCandidatParId(candidat.Id);

                candidatbdd.DateDeNaissance = candidat.DateDeNaissance;
                candidatbdd.Email = candidat.Email;
                candidatbdd.Commentaire = candidat.Commentaire;
                candidatbdd.Nom = candidat.Nom;
                candidatbdd.Prenom = candidat.Prenom;
                candidatbdd.Telephone = candidat.Telephone;

                gdd.DepotCandidats.Update(candidatbdd);
                gdd.DepotCandidats.PersistChanges();
            }
            if (!String.IsNullOrWhiteSpace(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Details", "Candidat", new { id = candidat.Id });
        }


    }
}