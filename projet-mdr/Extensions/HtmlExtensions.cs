﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Extensions
{
    public static class ExtensionMethods
    {
        public static MvcHtmlString BoutonsStatut(this HtmlHelper html, string statut, string id)
        {

            return MvcHtmlString.Create(String.Format(@"
                            <a onclick='changerstatut(""retenu"",{0})' class=""btn"" data-toggle=""tooltip"" data-placement=""bottom"" title=""Valider la candidature""><span class=""glyphicon glyphicon-ok"" aria-hidden=""true"" style=""font-size:20px;color:green""></span></a>
                            <a onclick='changerstatut(""à réétudier"",{0})' class=""btn"" data-toggle=""tooltip"" data-placement=""bottom"" title=""Candidature à revoir""><span class=""glyphicon glyphicon-question-sign"" aria-hidden=""true"" style=""font-size:20px;color:orange""></span></a>
                            <a onclick='changerstatut(""refusé"",{0})' class=""btn"" data-toggle=""tooltip"" data-placement=""bottom"" title=""Refuser la candidature""><span class=""glyphicon glyphicon-remove"" aria-hidden=""true"" style=""font-size:20px;color:red""></span></a>
                            <a onclick='changerstatut(""archivé"",{0})' class=""btn"" data-toggle=""tooltip"" data-placement=""bottom"" title=""Archiver la candidature""><span class=""glyphicon glyphicon-file"" aria-hidden=""true"" style=""font-size:20px""></span></a>
                    ", id));

        }
    }
}